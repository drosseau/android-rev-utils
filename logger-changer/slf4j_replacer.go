package loggerchanger

import (
	"fmt"
	"regexp"
)

type sl4jReplacer struct {
	debug   *regexp.Regexp
	info    *regexp.Regexp
	warn    *regexp.Regexp
	err     *regexp.Regexp
	verbose *regexp.Regexp
}

// SLF4JReplacer returns a Replacer that will replace all SLF4J loggers with
// custom loggers.
func SLF4JReplacer() Replacer {
	return new(sl4jReplacer)
}

func (sl *sl4jReplacer) Replace(from LogLevel, to LogLevel, in []byte) ([]byte, error) {
	b, err := DefaultRevUtilsToLogLevel(to)
	if b == nil {
		return nil, err
	}
	if err := sl.makeRegexp(from); err != nil {
		return nil, err
	}
	switch from {
	case LogLevelDebug:
		return sl.debug.ReplaceAll(in, b), nil
	case LogLevelInfo:
		return sl.info.ReplaceAll(in, b), nil
	case LogLevelWarning:
		return sl.warn.ReplaceAll(in, b), nil
	case LogLevelError:
		return sl.err.ReplaceAll(in, b), nil
	case LogLevelVerbose:
		return sl.verbose.ReplaceAll(in, b), nil
	default:
		return nil, fmt.Errorf("unknown loglevel for from: %v", from)
	}
}

func (sl *sl4jReplacer) compile(funcName string) (*regexp.Regexp, error) {
	format := `invoke-interface \{v\d+, ((?:v\d+,?\s?)+)\}, Lorg/slf4j/Logger;->%s\(Ljava/lang/String;((?:\[?(Ljava/lang/Object;)+)|(?:Ljava/lang/Throwable;))?\)V`
	return regexp.Compile(fmt.Sprintf(format, funcName))
}

func (sl *sl4jReplacer) makeRegexp(l LogLevel) error {
	var err error
	switch l {
	case LogLevelDebug:
		if sl.debug != nil {
			return nil
		}
		sl.debug, err = sl.compile("debug")
	case LogLevelInfo:
		if sl.info != nil {
			return nil
		}
		sl.info, err = sl.compile("info")
	case LogLevelWarning:
		if sl.warn != nil {
			return nil
		}
		sl.warn, err = sl.compile("warn")
	case LogLevelError:
		if sl.err != nil {
			return nil
		}
		sl.err, err = sl.compile("error")
	case LogLevelVerbose:
		if sl.verbose != nil {
			return nil
		}
		sl.verbose, err = sl.compile("verbose")
	default:
		return fmt.Errorf("unknown loglevel: %v", l)
	}
	return err
}
