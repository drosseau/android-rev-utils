package loggerchanger

import (
	"bytes"
	"fmt"
)

func TestRunner(repMaker func() Replacer, fromFunc func(LogLevel) []byte, toFunc func(LogLevel) []byte) error {
	rep := repMaker()
	for _, from := range AllLevels {
		for _, to := range AllLevels {
			b := fromFunc(from)
			got, err := rep.Replace(from, to, b)
			if err != nil {
				return err
			}
			exp := toFunc(to)
			if !bytes.Equal(got, exp) {
				return fmt.Errorf("got the wrong output:\nexpected=`%s`\ngot=`%s`", string(exp), string(got))
			}
		}
	}
	return nil
}

func FromFuncMaker(s string, funcNameGetter func(LogLevel) string) func(LogLevel) []byte {
	return func(l LogLevel) []byte {
		return []byte(fmt.Sprintf(s, funcNameGetter(l)))
	}
}

func ToFuncMaker(registers string, ending string) func(LogLevel) []byte {
	return func(l LogLevel) []byte {
		format := `invoke-static {%s}, Llocal/revutils/Utils;->%s(Ljava/lang/String;%s)V`
		f, err := RevUtilsFuncFromLogLevel(l)
		if err != nil {
			panic(err)
		}
		return []byte(fmt.Sprintf(format, registers, f, ending))
	}
}
