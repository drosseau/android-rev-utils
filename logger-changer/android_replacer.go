package loggerchanger

import (
	"fmt"
	"regexp"
)

type androidReplacer struct {
	debug   *regexp.Regexp
	info    *regexp.Regexp
	warn    *regexp.Regexp
	err     *regexp.Regexp
	verbose *regexp.Regexp
}

// AndroidReplacer returns a Replacer that will replace all default Android
// loggers with custom loggers.
func AndroidReplacer() Replacer {
	return new(androidReplacer)
}

func (droid *androidReplacer) Replace(from LogLevel, to LogLevel, in []byte) ([]byte, error) {
	b, err := DefaultRevUtilsToLogLevel(to)
	if b == nil {
		return nil, err
	}
	if err := droid.makeRegexp(from); err != nil {
		return nil, err
	}
	switch from {
	case LogLevelDebug:
		return droid.debug.ReplaceAll(in, b), nil
	case LogLevelInfo:
		return droid.info.ReplaceAll(in, b), nil
	case LogLevelWarning:
		return droid.warn.ReplaceAll(in, b), nil
	case LogLevelError:
		return droid.err.ReplaceAll(in, b), nil
	case LogLevelVerbose:
		return droid.verbose.ReplaceAll(in, b), nil
	default:
		return nil, fmt.Errorf("unknown loglevel for from: %v", from)
	}
}

func (droid *androidReplacer) compile(funcName string) (*regexp.Regexp, error) {
	format := `invoke-static \{v\d+, ([^\}]+)\}, Landroid/util/Log;->%s\(Ljava/lang/String;Ljava/lang/String;(Ljava/lang/Throwable;)?\)I`
	return regexp.Compile(fmt.Sprintf(format, funcName))
}

func (droid *androidReplacer) makeRegexp(l LogLevel) error {
	var err error
	switch l {
	case LogLevelDebug:
		if droid.debug != nil {
			return nil
		}
		droid.debug, err = droid.compile("d")
	case LogLevelInfo:
		if droid.info != nil {
			return nil
		}
		droid.info, err = droid.compile("i")
	case LogLevelWarning:
		if droid.warn != nil {
			return nil
		}
		droid.warn, err = droid.compile("w")
	case LogLevelError:
		if droid.err != nil {
			return nil
		}
		droid.err, err = droid.compile("e")
	case LogLevelVerbose:
		if droid.verbose != nil {
			return nil
		}
		droid.verbose, err = droid.compile("v")
	default:
		return fmt.Errorf("unknown loglevel: %v", l)
	}
	return err
}
