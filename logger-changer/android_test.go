package loggerchanger

import (
	"fmt"
	"testing"
)

func getAndroidFuncName(l LogLevel) string {
	switch l {
	case LogLevelDebug:
		return "d"
	case LogLevelInfo:
		return "i"
	case LogLevelWarning:
		return "w"
	case LogLevelError:
		return "e"
	case LogLevelVerbose:
		return "v"
	default:
		panic(fmt.Sprintf("unexpected log level: %v", l))
	}
}

func TestAndroidStringOnly(t *testing.T) {
	fromFunc := FromFuncMaker(`invoke-static {v0, v3}, Landroid/util/Log;->%s(Ljava/lang/String;Ljava/lang/String;)I`, getAndroidFuncName)

	toFunc := ToFuncMaker("v3", "")

	testRunner(t, AndroidReplacer, fromFunc, toFunc)
}

func TestAndroidStringThrowable(t *testing.T) {
	fromFunc := FromFuncMaker(`invoke-static {v0, v3, v4}, Landroid/util/Log;->%s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I`, getAndroidFuncName)

	toFunc := ToFuncMaker("v3, v4", `Ljava/lang/Throwable;`)

	testRunner(t, AndroidReplacer, fromFunc, toFunc)
}
