package loggerchanger

const (
	// PluginSymbolName is the name of the function that should return a
	// Replacer object
	PluginSymbolName = "PluginReplacerCreator"
)

// ReplacerCreator is the type the function should be
type ReplacerCreator func() Replacer
