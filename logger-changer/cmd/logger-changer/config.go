package main

import (
	"encoding/json"
	"fmt"
	"strings"

	loggerchanger "gitlab.com/drosseau/android-rev-utils/logger-changer"
)

type replacerConfig struct {
	creator loggerchanger.ReplacerCreator
	mapping map[loggerchanger.LogLevel]loggerchanger.LogLevel
}

func (r *replacerConfig) ensureCompleteMapping() {
	if r.mapping == nil {
		r.mapping = make(map[loggerchanger.LogLevel]loggerchanger.LogLevel)
	}
	for _, lev := range loggerchanger.AllLevels {
		if _, ok := r.mapping[lev]; !ok {
			r.mapping[lev] = lev
		}
	}
}

type config struct {
	replacers []replacerConfig
}

func (c *config) UnmarshalJSON(b []byte) error {
	m := make(map[string]map[string]string)
	err := json.Unmarshal(b, &m)
	if err != nil {
		return err
	}
	c.replacers = make([]replacerConfig, len(m))
	var i int
	for repName, levels := range m {
		c.replacers[i].creator, err = getReplacerCreator(repName)
		if err != nil {
			return err
		}
		c.replacers[i].mapping = make(map[loggerchanger.LogLevel]loggerchanger.LogLevel)
		for fromS, toS := range levels {
			from := loggerchanger.LogLevelFromString(fromS)
			if from == loggerchanger.LogLevelUnknown {
				return fmt.Errorf("unknown log level: %s", fromS)
			}
			to := loggerchanger.LogLevelFromString(toS)
			if to == loggerchanger.LogLevelUnknown {
				return fmt.Errorf("unknown log level: %s", toS)
			}
			c.replacers[i].mapping[from] = to
			c.replacers[i].ensureCompleteMapping()
			i++
		}
	}
	return nil
}

func (c *config) useReplacers(s string) error {
	split := strings.Split(s, ",")
	c.replacers = make([]replacerConfig, 0, len(split))
	for _, s := range split {
		cr, err := getReplacerCreator(s)
		if err != nil {
			return err
		}
		rep := replacerConfig{
			creator: cr,
		}
		rep.ensureCompleteMapping()
		c.replacers = append(c.replacers, rep)
	}
	return nil
}

func getReplacerCreator(s string) (loggerchanger.ReplacerCreator, error) {
	if rep, ok := allReplacers[s]; ok {
		return rep, nil
	}
	return fromPlugin(s)
}
