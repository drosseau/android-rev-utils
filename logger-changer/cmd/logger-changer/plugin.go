// +build linux darwin

package main

import (
	"fmt"
	"plugin"
	"strings"

	loggerchanger "gitlab.com/drosseau/android-rev-utils/logger-changer"
)

func fromPlugin(fname string) (loggerchanger.ReplacerCreator, error) {
	if !strings.HasSuffix(fname, ".so") {
		fname += ".so"
	}
	p, err := plugin.Open(fname)
	if err != nil {
		return nil, err
	}
	sym, err := p.Lookup(loggerchanger.PluginSymbolName)
	if err != nil {
		return nil, err
	}
	rep, ok := sym.(func() loggerchanger.Replacer)
	if !ok {
		return nil, fmt.Errorf("function %s in %s is not a ReplacerCreator", loggerchanger.PluginSymbolName, fname)
	}
	return loggerchanger.ReplacerCreator(rep), nil
}
