There is a whole bunch of craziness going on with plugins. If you use the Go
master branch on Github you don't need `work_darwin.go`. If you don't use
plugins you also don't need it. If you do need plugins -- I don't even think
that will save you. I'm considering just making plugins unsupported on darwin
for now.
