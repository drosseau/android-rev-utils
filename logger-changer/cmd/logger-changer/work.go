package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

// This is being build on darwin, but it will only work if you use the latest Go
// built from source on the master branch. Honestly, the _darwin.go option
// doesn't even seem to work with plugins either. Basically -- if you want
// Darwin and plugins just get bleeding edge Go.

func doWholeDirectory() {
	files, err := ioutil.ReadDir(*replacePath)
	if err != nil {
		panic(err)
	}
	var wg sync.WaitGroup
	ch := beginWorkers(&wg)
	doFiles(ch, files, *replacePath)
	close(ch)
	wg.Wait()
}

func beginWorkers(wg *sync.WaitGroup) chan<- string {
	wg.Add(*nWorkers)
	ch := make(chan string, 10)
	for i := 0; i < *nWorkers; i++ {
		go workerFunc(wg, ch, conf.replacers)
	}
	return ch
}

func workerFunc(wg *sync.WaitGroup, ch <-chan string, repConfs []replacerConfig) {
	reps := make([]replacer, len(repConfs))
	for i, c := range repConfs {
		reps[i] = replacer{
			rep:     c.creator(),
			mapping: c.mapping,
		}
	}
	defer wg.Done()
	var into [maxRead]byte
	var body []byte
	for fname := range ch {
		handleFileLowMem(fname, reps, into[:], body)
		body = body[:0]
	}
}
func doFiles(ch chan<- string, files []os.FileInfo, base string) {
	for _, f := range files {
		fname := filepath.Join(base, f.Name())
		if strings.HasSuffix(fname, ".smali") {
			ch <- fname
		} else if *recursive && f.IsDir() {
			d, err := os.Open(fname)
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				continue
			}
			for {
				newFiles, err := d.Readdir(10)
				if err != nil {
					if err == io.EOF {
						break
					}
					fmt.Fprintln(os.Stderr, err)
					break
				}
				doFiles(ch, newFiles, fname)
			}
			d.Close()
		}
	}
}
