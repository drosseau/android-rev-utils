// +build !linux,!darwin

package main

import (
	"errors"

	loggerchanger "gitlab.com/drosseau/android-rev-utils/logger-changer"
)

func fromPlugin(fname string) (loggerchanger.ReplacerCreator, error) {
	return nil, errors.New("plugins not supported on your OS")
}
