package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"os"

	loggerchanger "gitlab.com/drosseau/android-rev-utils/logger-changer"
)

var (
	replacePath  *string
	replaceFile  = flag.String("file", "", "Replace in this single file only.")
	conf         config
	replacers    = flag.String("replacers", "", "Comma separated list of replacers. If set you don't need to set a config file and default mappings are used.")
	configFile   = flag.String("config", "logger-changer-conf.json", "Replacers config file.")
	nWorkers     = flag.Int("n", 5, "Number of goroutines to use for workers.")
	recursive    = flag.Bool("r", false, "Recursively change files in other children dirs.")
	allReplacers = map[string]loggerchanger.ReplacerCreator{
		"slf4j":   loggerchanger.ReplacerCreator(loggerchanger.SLF4JReplacer),
		"android": loggerchanger.ReplacerCreator(loggerchanger.AndroidReplacer),
		"timber":  loggerchanger.ReplacerCreator(loggerchanger.TimberReplacer),
	}
)

const (
	maxRead = 1024
)

func init() {
	var def string
	pwd, err := os.Getwd()
	if err != nil {
		def = "."
	} else {
		def = pwd
	}
	replacePath = flag.String("replace-path", def, "Path to search for smali files to replace strings in.")
}

func main() {
	flag.Parse()
	if *replacers == "" {
		f, err := os.Open(*configFile)
		if err != nil {
			fmt.Fprintln(os.Stderr, "need to set -config or -replacers")
		}
		if err := json.NewDecoder(f).Decode(&conf); err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		f.Close()
	} else {
		if err := conf.useReplacers(*replacers); err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
	}
	if *replaceFile != "" {
		reps := make([]replacer, len(conf.replacers))
		for i, c := range conf.replacers {
			reps[i] = replacer{
				rep:     c.creator(),
				mapping: c.mapping,
			}
		}
		handleFile(*replaceFile, reps)
	} else {
		doWholeDirectory()
	}
}

type replacer struct {
	rep     loggerchanger.Replacer
	mapping map[loggerchanger.LogLevel]loggerchanger.LogLevel
}

func handleFile(fname string, reps []replacer) {
	var into [maxRead]byte
	var body []byte
	handleFileLowMem(fname, reps, into[:], body)
}

func handleFileLowMem(fname string, reps []replacer, into, body []byte) {
	f, err := os.Open(fname)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	for {
		n, err := f.Read(into[:])
		if err != nil {
			if err == io.EOF {
				break
			}
			fmt.Fprintln(os.Stderr, err)
			f.Close()
			return
		}
		body = append(body, into[:n]...)
	}
	f.Close()
	for _, rep := range reps {
		for from, to := range rep.mapping {
			body, err = rep.rep.Replace(from, to, body)
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				continue
			}
		}
	}
	// TODO: Should probably be copying the permissions
	fNew, err := os.OpenFile(fname, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	n, err := fNew.Write(body)
	if n != len(body) {
		fmt.Fprintln(os.Stderr, "short write")
	} else if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
	fNew.Close()
}
