// +build

package main

// This is incredibly inefficient due to issues with plugins on Darwin.
//
//    - I can't use goroutines.
//	   - I can't use recursive functions.
//
// In fact: this doesn't work at all still. Even with removing recursion and
// goroutines I crash a ton on go 1.10.2.
//
// TODO: Check to see if we're even using plugins. If not, use the non-Darwin
// algorithm. That one won't crash if plugins aren't being used.
//
// I'm so confused. I'm going to abandon all hope here. With Go and Mac and
// Plguins there be dragons.

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

func doWholeDirectory() {
	reps := make([]replacer, len(conf.replacers))
	for i, c := range conf.replacers {
		reps[i] = replacer{
			rep:     c.creator(),
			mapping: c.mapping,
		}
	}
	dirs := &[]string{*replacePath}
	for len(*dirs) > 0 {
		d := (*dirs)[0]
		*dirs = (*dirs)[1:]
		fmt.Println("Doing dir", d)
		*dirs = append(*dirs, doFilesInDir(d, reps)...)
	}
}

func doFilesInDir(dir string, reps []replacer) []string {
	var into [maxRead]byte
	var body []byte
	var dirs []string
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return dirs
	}
	for _, f := range files {
		fname := filepath.Join(dir, f.Name())
		if strings.HasSuffix(fname, ".smali") {
			handleFileLowMem(fname, reps, into[:], body)
		} else if *recursive && f.IsDir() {
			d, err := os.Open(fname)
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				continue
			}
			newFiles, err := d.Readdir(0)
			if err != nil {
				if err == io.EOF {
					continue
				}
				fmt.Fprintln(os.Stderr, err)
				d.Close()
				break
			}
			dirs = append(dirs, fname)
			fmt.Println("adding dir", fname)
			for _, f2 := range newFiles {
				if f2.IsDir() {
					fmt.Println("adding dir", filepath.Join(fname, f2.Name()))
					dirs = append(dirs, filepath.Join(fname, f2.Name()))
				}
			}
			d.Close()
		}
	}
	return dirs
}
