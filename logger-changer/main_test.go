package loggerchanger

import "testing"

// TODO Switch our testing to use the exported functions and don't have
// duplicate code

var (
	testLogLevels = []LogLevel{
		LogLevelDebug,
		LogLevelInfo,
		LogLevelWarning,
		LogLevelError,
	}
)

func testRunner(t *testing.T, repMaker func() Replacer, fromFunc func(LogLevel) []byte, toFunc func(LogLevel) []byte) {
	if err := TestRunner(repMaker, fromFunc, toFunc); err != nil {
		t.Fatal(err)
	}
}
