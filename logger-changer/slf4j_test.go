package loggerchanger

import (
	"fmt"
	"testing"
)

func getSLF4JFuncName(l LogLevel) string {
	switch l {
	case LogLevelDebug:
		return "debug"
	case LogLevelInfo:
		return "info"
	case LogLevelWarning:
		return "warn"
	case LogLevelError:
		return "error"
	case LogLevelVerbose:
		return "verbose"
	default:
		panic(fmt.Sprintf("unexpected log level: %v", l))
	}
}

func TestSLF4JTwoObjs(t *testing.T) {
	fromFunc := FromFuncMaker(`invoke-interface {v0, v3, v4}, Lorg/slf4j/Logger;->%s(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V`, getSLF4JFuncName)

	toFunc := ToFuncMaker("v3, v4", `Ljava/lang/Object;Ljava/lang/Object;`)

	testRunner(t, SLF4JReplacer, fromFunc, toFunc)
}

func TestSLF4JOneObj(t *testing.T) {
	fromFunc := FromFuncMaker(`invoke-interface {v0, v3, v4}, Lorg/slf4j/Logger;->%s(Ljava/lang/String;Ljava/lang/Object;)V`, getSLF4JFuncName)

	toFunc := ToFuncMaker("v3, v4", `Ljava/lang/Object;`)

	testRunner(t, SLF4JReplacer, fromFunc, toFunc)
}

func TestSLF4JObjArray(t *testing.T) {
	fromFunc := FromFuncMaker(`invoke-interface {v0, v3, v4}, Lorg/slf4j/Logger;->%s(Ljava/lang/String;[Ljava/lang/Object;)V`, getSLF4JFuncName)

	toFunc := ToFuncMaker("v3, v4", `[Ljava/lang/Object;`)

	testRunner(t, SLF4JReplacer, fromFunc, toFunc)
}

func TestSLF4JStringOnly(t *testing.T) {
	fromFunc := FromFuncMaker(`invoke-interface {v0, v3}, Lorg/slf4j/Logger;->%s(Ljava/lang/String;)V`, getSLF4JFuncName)

	toFunc := ToFuncMaker("v3", "")

	testRunner(t, SLF4JReplacer, fromFunc, toFunc)
}

func TestSLF4JStringThrowable(t *testing.T) {
	fromFunc := FromFuncMaker(`invoke-interface {v0, v3, v5}, Lorg/slf4j/Logger;->%s(Ljava/lang/String;Ljava/lang/Throwable;)V`, getSLF4JFuncName)

	toFunc := ToFuncMaker("v3, v5", `Ljava/lang/Throwable;`)

	testRunner(t, SLF4JReplacer, fromFunc, toFunc)
}
