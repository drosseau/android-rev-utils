package loggerchanger

import (
	"fmt"
	"regexp"
)

type timberReplacer struct {
	debug   *regexp.Regexp
	info    *regexp.Regexp
	warn    *regexp.Regexp
	err     *regexp.Regexp
	verbose *regexp.Regexp
}

func TimberReplacer() Replacer {
	return new(timberReplacer)
}

func customTimberToLogLevel(to LogLevel) ([]byte, error) {
	f, err := RevUtilsFuncFromLogLevel(to)
	if err != nil {
		return nil, err
	}
	return []byte(fmt.Sprintf(
		"invoke-static {$1}, Llocal/revutils/Utils;->%s(${2}${3})V", f),
	), nil
}

func (t *timberReplacer) Replace(from LogLevel, to LogLevel, in []byte) ([]byte, error) {
	b, err := customTimberToLogLevel(to)
	if err != nil {
		return nil, err
	}

	if err := t.makeRegexp(from); err != nil {
		return nil, err
	}

	switch from {
	case LogLevelDebug:
		return t.debug.ReplaceAll(in, b), nil
	case LogLevelInfo:
		return t.info.ReplaceAll(in, b), nil
	case LogLevelWarning:
		return t.warn.ReplaceAll(in, b), nil
	case LogLevelError:
		return t.err.ReplaceAll(in, b), nil
	case LogLevelVerbose:
		return t.verbose.ReplaceAll(in, b), nil
	default:
		return nil, fmt.Errorf("unknown loglevel for from: %v", from)
	}
}

func (t *timberReplacer) compile(funcName string) (*regexp.Regexp, error) {
	format := `invoke-static\s*\{((?:v\d+,?\s*)+)\},\s*Ltimber/log/Timber;->%s\((Ljava/lang/Throwable;)?(Ljava/lang/String;\[Ljava/lang/Object;)?\)V`
	return regexp.Compile(fmt.Sprintf(format, funcName))
}

func (t *timberReplacer) makeRegexp(l LogLevel) error {
	var err error
	switch l {
	case LogLevelDebug:
		if t.debug != nil {
			return nil
		}
		t.debug, err = t.compile("d")
	case LogLevelInfo:
		if t.info != nil {
			return nil
		}
		t.info, err = t.compile("i")
	case LogLevelWarning:
		if t.warn != nil {
			return nil
		}
		t.warn, err = t.compile("w")
	case LogLevelError:
		if t.err != nil {
			return nil
		}
		t.err, err = t.compile("e")
	case LogLevelVerbose:
		if t.verbose != nil {
			return nil
		}
		t.verbose, err = t.compile("v")
	default:
		return fmt.Errorf("unknown loglevel: %v", l)
	}
	return err
}
