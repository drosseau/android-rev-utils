package loggerchanger

import "fmt"

//go:generate stringer -type LogLevel

// LogLevel identifies different log levels available
type LogLevel uint8

const (
	// LogLevelUnknown covers the default case when we don't know the log level
	LogLevelUnknown LogLevel = iota
	// LogLevelDebug targets debug level logging
	LogLevelDebug
	// LogLevelInfo targets info level logging
	LogLevelInfo
	// LogLevelWarning targets warning level logging
	LogLevelWarning
	// LogLevelError targets error level logging
	LogLevelError
	// LogLevelVerbose targets verbose level logging
	LogLevelVerbose
)

var (
	// AllLevels holds all available levels
	AllLevels = [5]LogLevel{
		LogLevelDebug,
		LogLevelInfo,
		LogLevelWarning,
		LogLevelError,
		LogLevelVerbose,
	}
)

// LogLevelFromString returns the LogLevel for the given string. The strings
// should be:
//
//    "debug"
//    "info"
//    "warning"
//    "error"
func LogLevelFromString(s string) LogLevel {
	switch s {
	case "debug":
		return LogLevelDebug
	case "info":
		return LogLevelInfo
	case "warning":
		return LogLevelWarning
	case "error":
		return LogLevelError
	case "verbose":
		return LogLevelVerbose
	}
	return LogLevelError
}

// RevUtilsFuncFromLogLevel returns the appropriate RevUtils function for the
// given log level
func RevUtilsFuncFromLogLevel(l LogLevel) (string, error) {
	switch l {
	case LogLevelDebug:
		return "logDebugTagged", nil
	case LogLevelInfo:
		return "logInfoTagged", nil
	case LogLevelWarning:
		return "logWarningTagged", nil
	case LogLevelError:
		return "logErrorTagged", nil
	case LogLevelVerbose:
		return "logVerboseTagged", nil
	default:
		return "", fmt.Errorf("unknown log level: %v", l)
	}
}

// DefaultRevUtilsToLogLevel builds the default byte slice that you'll probably
// want to transform things into for a given log level.
//
// The output is:
// invoke-static {$1}, Llocal/revutils/Utils;->{TO_FUNC}(Ljava/lang/String;$2)V
//
// Placing your first regexp capture as the registers and second as some extras
// on the Utils call.
func DefaultRevUtilsToLogLevel(to LogLevel) ([]byte, error) {
	f, err := RevUtilsFuncFromLogLevel(to)
	if err != nil {
		return nil, err
	}
	return []byte(fmt.Sprintf(
		"invoke-static {$1}, Llocal/revutils/Utils;->%s(Ljava/lang/String;$2)V", f),
	), nil
}

// A Replacer replaces parts of byte slices with a new value.
// For instance a Regexp could be a replacer.
type Replacer interface {
	// Replace will should From level logging into To level logging in the
	// given RuneReader.
	Replace(from LogLevel, to LogLevel, in []byte) ([]byte, error)
}
