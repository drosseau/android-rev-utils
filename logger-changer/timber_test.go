package loggerchanger

import (
	"fmt"
	"testing"
)

func timberToFuncMaker(registers string, args string) func(LogLevel) []byte {
	return func(l LogLevel) []byte {
		format := `invoke-static {%s}, Llocal/revutils/Utils;->%s(%s)V`
		f, err := RevUtilsFuncFromLogLevel(l)
		if err != nil {
			panic(err)
		}
		return []byte(fmt.Sprintf(format, registers, f, args))
	}
}

func getTimberFuncName(l LogLevel) string {
	switch l {
	case LogLevelDebug:
		return "d"
	case LogLevelInfo:
		return "i"
	case LogLevelWarning:
		return "w"
	case LogLevelError:
		return "e"
	case LogLevelVerbose:
		return "v"
	default:
		panic(fmt.Sprintf("unexpected log level: %v", l))
	}
}

func TestTimberObjArray(t *testing.T) {
	fromFunc := FromFuncMaker(`invoke-static {v3, v4}, Ltimber/log/Timber;->%s(Ljava/lang/String;[Ljava/lang/Object;)V`, getTimberFuncName)
	toFunc := timberToFuncMaker("v3, v4", `Ljava/lang/String;[Ljava/lang/Object;`)

	testRunner(t, TimberReplacer, fromFunc, toFunc)
}

func TestTimberAll(t *testing.T) {
	fromFunc := FromFuncMaker(`invoke-static {v3, v4, v5}, Ltimber/log/Timber;->%s(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V`, getTimberFuncName)

	toFunc := timberToFuncMaker("v3, v4, v5", "Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;")

	testRunner(t, TimberReplacer, fromFunc, toFunc)
}

func TestTimberStringThrowableOnly(t *testing.T) {
	fromFunc := FromFuncMaker(`invoke-static {v0}, Ltimber/log/Timber;->%s(Ljava/lang/Throwable;)V`, getTimberFuncName)

	toFunc := timberToFuncMaker("v0", `Ljava/lang/Throwable;`)

	testRunner(t, TimberReplacer, fromFunc, toFunc)
}
