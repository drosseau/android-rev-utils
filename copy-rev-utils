#!/bin/bash

# Requires gradle

if [ -z "$1" ]; then
    target="$(pwd)"
    d=$(basename "$target")
    if [ "$d" != "smali" ]; then
        echo "[-] No target passed in \$1 and $target is not a \`smali\` dir"
        exit 1
    fi
else
    target="$1"
fi

fname="Utils.smali"
smalipath="local/revutils"

appdir="$GOPATH/src/gitlab.com/drosseau/android-rev-utils/androidapp/app"
srcdir="$appdir/src/main/java/local/revutils/"
dir="$appdir/build/outputs/apk/debug"
# MODIFY THIS TO SUIT YOUR NEEDS
# for macOS
#statmod='stat -f "%m"'
# For Linux
statmod='stat -c "%Y"'

if [ ! -d "$dir" ]; then
    echo "[-] $dir doesn't exist"
    exit 1
fi

apkf="$dir/app-debug.apk"

utilsfile="$dir/unpacked/smali/$smalipath/$fname"

if [ -f "$apkf" ]; then
    apkmodified=$(eval "$statmod $apkf")
else
    apkmodified="0"
fi

if [ -d "$srcdir" ]; then
    srcmodified=$(eval "$statmod $srcdir")
    if [ ! -f "$apkf" ] || [ "$srcmodified" -gt "$apkmodified" ]; then
        echo "[+] Building apk"
        if [ -f "$apkf" ]; then
            rm "$apkf"
        fi
        # I always forget if I need to move back to OLDPWD
        cd "$appdir/.." && gradle build && cd "$OLDPWD"
        if [ "$?" -ne "0" ]; then
            echo "[-] Rebuilding failed"
            exit 1
        fi
    fi
fi

if [ ! -f "$apkf" ]; then
    echo "[-] Couldn't build or find the apk file: $apkf"
    exit 1
fi

smalimodified=$(eval "$statmod $utilsfile")
apkmodified=$(eval "$statmod $apkf")

if [ ! -f "$utilsfile" ] || [ "$smalimodified" -lt "$apkmodified" ]; then
    echo "[+] Running apktool to get smali"
    apktool d "$apkf" -o "$dir/unpacked" -f
    if [ "$?" -ne "0" ]; then
        echo "[-] apktool failed"
        exit 1
    fi
fi

if [ ! -d "$target/$smalipath" ]; then
    mkdir -p "$target/$smalipath"
fi

for f in $(find "$dir/unpacked/smali/$smalipath" -type f -name '*.smali'); do
    to=$(basename "$f")
    if [ "${to:0:2}" != "R\$" ] && [ "$to" != "R.smali" ] && [ "$to" != "BuildConfig.smali" ]; then
        cp "$f" "$target/$smalipath/$to"
        echo "[+] Copied $f to $target/$smalipath/$to"
    fi
done
