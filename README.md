The included Android app isn't an actual app. It is just used to build the APK
that we then grab the smali out of. There might be an easier way, but I need the
Android SDK and this works.

Requires gradle >= 4.4
