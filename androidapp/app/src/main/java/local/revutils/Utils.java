package local.revutils;

import android.util.Log;

import android.os.Bundle;
import android.os.Environment;

import android.content.Intent;
import android.content.IntentFilter;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.BufferedWriter;

import java.lang.Iterable;
import java.lang.StringBuilder;

import java.nio.ByteBuffer;

import java.util.Set;
import java.util.UUID;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

/**
 * This class is intended to be dropped into an APK that was decompiled with
 * apktool to smali and then can be rebuilt.
 */
public class Utils {
	 private static String LOG_TAG = "RevUtils";
	 private static final int CHUNK = 2048;
	 private static final char[] HEX_CHARS = "0123456789abcdef".toCharArray();

	 public static void dumpStackTrace() {
		  StackTraceElement[] elems = Thread.currentThread().getStackTrace();
		  String myClass = Utils.class.getName();
		  int i = 0;
		  while(i < elems.length) {
				StackTraceElement el = elems[i];
				String cls = el.getClassName();
				if (cls.equals(myClass)) {
					 break;
				}
				i++;
		  }
		  while(i < elems.length) {
				StackTraceElement el = elems[i];
				String cls = el.getClassName();
				if (!(cls.equals(myClass))) {
					 break;
				}
				i++;
		  }
		  for (; i < elems.length; i++) {
				doLog(Tag.INFO, String.format("%s", elems[i]));
		  }
	 }

	 static void doLog(Tag tag, String s) {
		  doLog(LOG_TAG, tag, s, null);
	 }

	 static void doLog(String lt, Tag tag, String s) {
		  doLog(lt, tag, s, null);
	 }

     static void doLog(Tag tag, String s, Throwable e) {
         doLog(LOG_TAG, tag, s, e);
     }

	 static void doLog(String lt, Tag tag, String s, Throwable e) {
		  StackTraceElement[] elems = Thread.currentThread().getStackTrace();
		  String myClass = Utils.class.getName();
		  int i = 0;
		  while(i < elems.length) {
				StackTraceElement el = elems[i];
				String cls = el.getClassName();
				if (cls.equals(myClass)) {
					 break;
				}
				i++;
		  }
		  String callerInfo = "[Unknown caller info]";
		  while(i < elems.length) {
				StackTraceElement el = elems[i];
				String cls = el.getClassName();
				if (!(cls.equals(myClass))) {
					 callerInfo = String.format(
						  "[%s.%s(...)@%s:%d]", el.getClassName(),
						  el.getMethodName(), el.getFileName(), el.getLineNumber()
					 );
					 break;
				}
				i++;
		  }
		  s = String.format("%s %s", callerInfo, s);
		  // Logcat can't handle messages longer than a certain size. That size
		  // is around 4096, but let's steer clear of it for now and break our
		  // message up into much smaller chunks.
		  if (!(s.length() > CHUNK)) {
				switch (tag) {
					 case DEBUG:
						  if (e != null) {
								Log.d(lt, s, e);
						  } else {
								Log.d(lt, s);
						  }
						  break;
					 case INFO:
						  if (e != null) {
								Log.i(lt, s, e);
						  } else {
								Log.i(lt, s);
						  }
						  break;
					 case WARNING:
						  if (e != null) {
								Log.w(lt, s, e);
						  } else {
								Log.w(lt, s);
						  }
						  break;
					 case ERROR:
						  if (e != null) {
								Log.e(lt, s, e);
						  } else {
								Log.e(lt, s);
						  }
						  break;
                    case VERBOSE:
                          if (e != null) {
                              Log.v(lt, s, e);
                          } else {
                              Log.v(lt, s);
                          }
                          break;
				}
				return;
		  }
		  i = 0;
		  List<String> strings = new ArrayList<String>(s.length() / CHUNK + 1);
		  while (i + CHUNK < s.length()) {
				strings.add(s.substring(i, i + CHUNK));
				i += CHUNK;
		  }
		  if (i != s.length()) {
				strings.add(s.substring(i));
		  }
		  int j = strings.size();
		  for (i = 0; i < j; i++) {
				String toLog = String.format("[%d/%d] - %s", i + 1, j, strings.get(i));
				switch (tag) {
					 case DEBUG:
						  if (i == j - 1 && e != null) {
								Log.d(lt, toLog, e);
						  } else {
								Log.d(lt, toLog);
						  }
						  break;
					 case INFO:
						  if (i == j - 1 && e != null) {
								Log.i(lt, toLog, e);
						  } else {
								Log.i(lt, toLog);
						  }
						  break;
					 case WARNING:
						  if (i == j - 1 && e != null) {
								Log.w(lt, toLog, e);
						  } else {
								Log.w(lt, toLog);
						  }
						  break;
					 case ERROR:
						  if (i == j - 1 && e != null) {
								Log.e(lt, toLog, e);
						  } else {
								Log.e(lt, toLog);
						  }
						  break;
                    case VERBOSE:
                          if (i == j - 1 && e != null) {
                              Log.v(lt, toLog, e);
                          } else {
                              Log.v(lt, toLog);
                          }
                          break;
				}
		  }
	 }

	 public static void dumpIntentFilter(IntentFilter filter) {
		  doLog(Tag.INFO, "Dumping IntentFilter");
		  if (filter == null) {
				doLog(Tag.INFO, "IntentFilter was null");
				return;
		  }
		  dumpIt(filter.actionsIterator(), "Action: %s");
		  dumpIt(filter.categoriesIterator(), "Category: %s");
		  dumpIt(filter.pathsIterator(), "Path: %s");
		  dumpIt(filter.schemesIterator(), "Scheme: %s");
		  dumpIt(filter.schemeSpecificPartsIterator(), "Scheme Spec Part: %s");
		  dumpIt(filter.typesIterator(), "Type: %s");
		  dumpIt(filter.authoritiesIterator(), "Authority: %s");
	 }

	 private static <T> void dumpIt(Iterator<T> it, String fmt) {
		  if (it != null && it.hasNext()) {
				for (T e : makeIterable(it)) {
					 doLog(Tag.INFO, String.format(fmt, e));
				}
		  }
	 }

	 private static <T> Iterable<T> makeIterable(final Iterator<T> it) {
		  return new Iterable<T>() { public Iterator<T> iterator() { return it; } };
	 }

	 public static void dumpIntent(Intent intent) {
		  doLog(Tag.INFO, "Dumping intent");
		  if (intent == null) {
				doLog(Tag.INFO, "Intent was null");
				return;
		  }
		  if (intent.getAction() != null) {
				doLog(Tag.INFO, String.format("Action: %s", intent.getAction()));
		  }
		  if (intent.getDataString() != null) {
				doLog(Tag.INFO, String.format("Data: %s", intent.getDataString()));
		  }
		  if (intent.getPackage() != null) {
				doLog(Tag.INFO, String.format("Package: %s", intent.getPackage()));
		  }
		  Set<String> categories = intent.getCategories();
		  if (categories != null && categories.size() > 0) {
				doLog(Tag.INFO, "Categories:");
				for (String s : categories) {
					 doLog(Tag.INFO, String.format("\t%s", s));
				}
		  }
		  Bundle bundle = intent.getExtras();
		  if (bundle != null) {
				Set<String> keys = bundle.keySet();
				if (keys != null && keys.size() > 0) {
					 doLog(Tag.INFO, "Extras:");
					 for (String k : keys) {
						  Object val = bundle.get(k);
						  if (val != null) {
								doLog(Tag.INFO, String.format(
									 "Key: %s, Val: %s, Class: %s",
									 k, val.toString(), val.getClass().getName()
								));
						  } else {
								doLog(Tag.INFO, String.format("Key %s is null", k));
						  }
					 }
				}
		  }
	 }


	 public static void logUuids(UUID[] uuids) {
		  logUuids("UUID is", uuids);
	 }

	 public static void logUuids(String pre, UUID[] uuids) {
		  for (UUID uuid : uuids) {
				String s = String.format("%s: %s", pre, uuid);
				doLog(Tag.INFO, s);
		  }
	 }

	 public static void logBytes(String s, ByteBuffer buf) {
		  logBytes(s, buf.array());
	 }

	 public static void logBytes(ByteBuffer buf) {
		  logBytes(buf.array());
	 }

	 public static void logBytes(byte[] b) {
		  logBytes("Bytes are", b);
	 }

	 public static void logBytes(String s, byte[] b) {
		  String out = String.format("%s: %s", s, bytesToHexString(b));
		  doLog(Tag.INFO, out);
	 }

	 public static void logWarningTagged(String s) {
		  logTagged(Tag.WARNING, s);
	 }

	 public static void logInfoTagged(String s) {
		  logTagged(Tag.INFO, s);
	 }

	 public static void logErrorTagged(String s) {
		  logTagged(Tag.ERROR, s);
	 }

	 public static void logDebugTagged(String s) {
		  logTagged(Tag.DEBUG, s);
	 }

	 public static void logVerboseTagged(String s) {
		  logTagged(Tag.VERBOSE, s);
	 }

	 public static void logWarningTagged(Throwable e) {
		  logTagged(Tag.WARNING, "", e);
	 }

	 public static void logInfoTagged(Throwable e) {
		  logTagged(Tag.INFO, "", e);
	 }

	 public static void logErrorTagged(Throwable e) {
		  logTagged(Tag.ERROR, "", e);
	 }

	 public static void logDebugTagged(Throwable e) {
		  logTagged(Tag.DEBUG, "", e);
	 }

	 public static void logVerboseTagged(Throwable e) {
		  logTagged(Tag.VERBOSE, "", e);
	 }

	 public static void logDebugTagged(String s, Throwable e) {
		  logTagged(Tag.DEBUG, s, e);
	 }

	 public static void logVerboseTagged(String s, Throwable e) {
		  logTagged(Tag.VERBOSE, s, e);
	 }

	 public static void logInfoTagged(String s, Throwable e) {
		  logTagged(Tag.INFO, s, e);
	 }

	 public static void logWarningTagged(String s, Throwable e) {
		  logTagged(Tag.WARNING, s, e);
	 }

	 public static void logErrorTagged(String s, Throwable e) {
		  logTagged(Tag.ERROR, s, e);
	 }

	 public static void logDebugTagged(Throwable e, String s) {
		  logTagged(Tag.DEBUG, s, e);
	 }

	 public static void logVerboseTagged(Throwable e, String s) {
		  logTagged(Tag.VERBOSE, s, e);
	 }

	 public static void logInfoTagged(Throwable e, String s) {
		  logTagged(Tag.INFO, s, e);
	 }

	 public static void logWarningTagged(Throwable e, String s) {
		  logTagged(Tag.WARNING, s, e);
	 }

	 public static void logErrorTagged(Throwable e, String s) {
		  logTagged(Tag.ERROR, s, e);
	 }

	 public static void logWarningTagged(String fmt, Object... objs) {
		  logTagged(Tag.WARNING, fmt, objs);
	 }

	 public static void logInfoTagged(String fmt, Object... objs) {
		  logTagged(Tag.INFO, fmt, objs);
	 }

	 public static void logErrorTagged(String fmt, Object... objs) {
		  logTagged(Tag.ERROR, fmt, objs);
	 }

	 public static void logDebugTagged(String fmt, Object... objs) {
		  logTagged(Tag.DEBUG, fmt, objs);
	 }

	 public static void logVerboseTagged(String fmt, Object... objs) {
		  logTagged(Tag.VERBOSE, fmt, objs);
	 }

	 public static void logDebugTagged(Throwable e, String fmt, Object... objs) {
		  logTagged(Tag.DEBUG, fmt, e, objs);
	 }

	 public static void logVerboseTagged(Throwable e, String fmt, Object... objs) {
		  logTagged(Tag.VERBOSE, fmt, e, objs);
	 }

	 public static void logInfoTagged(Throwable e, String fmt, Object... objs) {
		  logTagged(Tag.INFO, fmt, e, objs);
	 }

	 public static void logWarningTagged(Throwable e, String fmt, Object... objs) {
		  logTagged(Tag.WARNING, fmt, e, objs);
	 }

	 public static void logErrorTagged(Throwable e, String fmt, Object... objs) {
		  logTagged(Tag.ERROR, fmt, e, objs);
	 }

	 public static void logDebugTagged(String fmt, Throwable e, Object... objs) {
		  logTagged(Tag.DEBUG, fmt, e, objs);
	 }

	 public static void logVerboseTagged(String fmt, Throwable e, Object... objs) {
		  logTagged(Tag.VERBOSE, fmt, e, objs);
	 }

	 public static void logInfoTagged(String fmt, Throwable e, Object... objs) {
		  logTagged(Tag.INFO, fmt, e, objs);
	 }

	 public static void logWarningTagged(String fmt, Throwable e, Object... objs) {
		  logTagged(Tag.WARNING, fmt, e, objs);
	 }

	 public static void logErrorTagged(String fmt, Throwable e, Object... objs) {
		  logTagged(Tag.ERROR, fmt, e, objs);
	 }

	 public static void logTagged(Tag tag, String s) {
		  if (s == null) {
				doLog(Tag.ERROR, "given a null string to log");
				return;
		  }
		  doLog(tag, s);
	 }
	 public static void logTagged(Tag tag, String s, Throwable e) {
		  if (s == null) {
				doLog(Tag.ERROR, "given a null string to log");
				return;
		  }
		  doLog(tag, s, e);
	 }

	 public static void logTagged(Tag tag, String fmt, Object... objs) {
		  if (fmt == null) {
				doLog(Tag.ERROR, "given a null fmt string");
				return;
		  }
		  String out = String.format(fmt, objs);
		  doLog(tag, out);
	 }

	 public static void logTagged(Tag tag, String fmt, Throwable e, Object... objs) {
		  if (fmt == null) {
				doLog(Tag.ERROR, "given a null fmt string");
				return;
		  }
		  String out = String.format(fmt, objs);
		  doLog(tag, out, e);
	 }

	 public static void logf(String fmt, Object... objs) {
		  if (fmt == null) {
				doLog(Tag.ERROR, "given a null fmt string");
				return;
		  }
		  String out = String.format(fmt, objs);
		  doLog(Tag.INFO, out);
	 }

	 public static void logByte(int idx, byte[] bs) {
         if (idx < 0 || idx >= bs.length) {
             doLog(Tag.ERROR, String.format("Can't log byte at %d", idx));
             return;
         }
         logByte(idx, bs[idx]);
	 }

	 public static void logByte(int idx, byte b) {
		  char[] hex = {
				HEX_CHARS[b >> 4],
				HEX_CHARS[b & 0xF],
		  };
		  String out = String.format("Byte at %d is %s", idx, new String(hex));
		  doLog(Tag.INFO, out);
	 }

	 public static String bytesToHexString(byte[] bs) {
		  char[] hex = new char[bs.length * 2];
		  for (int i = 0; i < bs.length; i++) {
				int bv = bs[i] & 0xFF;
				hex[i << 1] = HEX_CHARS[bv >> 4];
				hex[(i << 1) + 1] = HEX_CHARS[bv & 0xF];
		  }
		  return new String(hex);
	 }

	 /**
	  * These functions help deal with SLF4J trying to be clever. They don't
	  * cover everything though. Check here:
	  * https://www.slf4j.org/apidocs/index.html
	  */
	 public static void logWarningTagged(String fmt, Object arg1) {
		  if (fmt == null) {
				doLog(Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logWarningTagged(String.format(fmt, arg1));
	 }
	 public static void logWarningTagged(String fmt, Object arg1, Object arg2) {
		  if (fmt == null) {
				doLog(Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logWarningTagged(String.format(fmt, arg1, arg2));
	 }
	 public static void logErrorTagged(String fmt, Object arg1) {
		  if (fmt == null) {
				doLog(Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logErrorTagged(String.format(fmt, arg1));
	 }
	 public static void logErrorTagged(String fmt, Object arg1, Object arg2) {
		  if (fmt == null) {
				doLog(Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logErrorTagged(String.format(fmt, arg1, arg2));
	 }
	 public static void logInfoTagged(String fmt, Object arg1) {
		  if (fmt == null) {
				doLog(Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logInfoTagged(String.format(fmt, arg1));
	 }
	 public static void logInfoTagged(String fmt, Object arg1, Object arg2) {
		  if (fmt == null) {
				doLog(Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logInfoTagged(String.format(fmt, arg1, arg2));
	 }

	 public static void logDebugTagged(String fmt, Object arg1) {
		  if (fmt == null) {
				doLog(Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logDebugTagged(String.format(fmt, arg1));
	 }
	 public static void logDebugTagged(String fmt, Object arg1, Object arg2) {
		  if (fmt == null) {
				doLog(Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logDebugTagged(String.format(fmt, arg1, arg2));
	 }

	 public static void logVerboseTagged(String fmt, Object arg1) {
		  if (fmt == null) {
				doLog(Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logVerboseTagged(String.format(fmt, arg1));
	 }
	 public static void logVerboseTagged(String fmt, Object arg1, Object arg2) {
		  if (fmt == null) {
				doLog(Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logVerboseTagged(String.format(fmt, arg1, arg2));
	 }


	 /**
	  * Searches for known gadgets for object deserialization attacks
	  *
	  * This doesn't attempt to find new ones, and I don't claim to have
	  * all of them known in here. If a gadget chain is discovered it will
	  * be dumped into Environment.getDataDirectory() with the name
	  * "discovered_gadget_chains".
	  *
	  * This method returns true if a gadget chain was found.
	  */
	 public static boolean discoverGadgetChain() {
		  StringBuilder builder = new StringBuilder(
				Environment.getDataDirectory().getPath()
		  );
		  builder.append(File.pathSeparator);
		  builder.append("discovered_gadget_chains");
		  return discoverGadgetChain(builder.toString());
	 }

	 public static boolean discoverGadgetChain(String p) {
		  boolean discovered = new GadgetDumper().dumpGadgets(p);
          if (discovered) {
              logInfoTagged("Gadget chain discovered! Check %s", p);
          }
          return discovered;
	 }

	 /**
	  * GadgetDumper is just based on ysoserial's known gadgets.
	  *
	  * This might not be particularly useful if classes are being obfuscated.
	  * We'd need to map this to the obfuscated name. That might be possible
	  * by analyzing .dex or .smali files, but that is out of the scope of this
	  * particular tool.
	  */
	 public static class GadgetDumper {
          private static final String LOG_TAG = "RevUtils-GadgetDumper";
		  private final ClassLoader mClassLoader;
		  private static final GadgetChain[] CHAINS = {
				new GadgetChain(
					 "ysoserial.CommmonsCollections1",
					 new String[] {"commons-collections:commons-collections:3.1"},
					 new String[] {
						  "org.apache.commons.collections.Transformer",
						  "org.apache.commons.collections.functors.ChainedTransformer",
						  "org.apache.commons.collections.functors.ConstantTransformer",
						  "org.apache.commons.collections.functors.InvokerTransformer",
						  "org.apache.commons.collections.map.LazyMap"
					 }
				),
				new GadgetChain(
					 "ysoserial.CommonsCollections2",
					 new String[] {"org.apache.commons:commons-collections4:4.0"},
					 new String[] {
						  "org.apache.commons.collections4.comparators.TransformingComparator",
						  "org.apache.commons.collections4.functors.InvokerTransformer"
					 }
				),
				new GadgetChain(
					 "ysoserial.CommonsCollections3",
					 new String[] {"commons-collections:commons-collections:3.1"},
					 new String[] {
						  "org.apache.commons.collections.Transformer",
						  "org.apache.commons.collections.functors.ChainedTransformer",
						  "org.apache.commons.collections.functors.ConstantTransformer",
						  "org.apache.commons.collections.functors.InstantiateTransformer",
						  "org.apache.commons.collections.map.LazyMap"
					 }
				),
				new GadgetChain(
					 "ysoserial.CommonsCollections4",
					 new String[] {"org.apache.commons:commons-collections4:4.0"},
					 new String[] {
		  				"org.apache.commons.collections4.Transformer",
		  				"org.apache.commons.collections4.comparators.TransformingComparator",
		  				"org.apache.commons.collections4.functors.ChainedTransformer",
		  				"org.apache.commons.collections4.functors.ConstantTransformer",
		  				"org.apache.commons.collections4.functors.InstantiateTransformer"
		  		  }
				),
				new GadgetChain(
					 "ysoserial.CommonsCollections5",
					 new String[] {"commons-collections:commons-collections:3.1"},
					 new String[] {
						  "org.apache.commons.collections.Transformer",
						  "org.apache.commons.collections.functors.ChainedTransformer",
						  "org.apache.commons.collections.functors.ConstantTransformer",
						  "org.apache.commons.collections.functors.InvokerTransformer",
						  "org.apache.commons.collections.keyvalue.TiedMapEntry",
						  "org.apache.commons.collections.map.LazyMap"
					 }
				),
				new GadgetChain(
					 "ysoserial.CommonsCollections6",
					 new String[] {"commons-collections:commons-collections:3.1"},
					 new String[] {
						  "org.apache.commons.collections.Transformer",
						  "org.apache.commons.collections.functors.ChainedTransformer",
						  "org.apache.commons.collections.functors.ConstantTransformer",
						  "org.apache.commons.collections.functors.InvokerTransformer",
						  "org.apache.commons.collections.keyvalue.TiedMapEntry",
						  "org.apache.commons.collections.map.LazyMap"
					 }
				)
		  };
		  GadgetDumper() {
				mClassLoader = Thread.currentThread().getContextClassLoader();
		  }

		  public boolean dumpGadgets(String p) {
				if (mClassLoader == null) {
					 Log.e(LOG_TAG, "No ClassLoader given to GadgetDumper");
					 return false;
				}
				boolean found = false;
				for (GadgetChain c : CHAINS) {
					 if (chainExists(c)) {
						  dumpChainToFile(p, c);
						  found = true;
					 }
				}
				return found;
		  }

		  private boolean chainExists(GadgetChain c) {
				ClassLoader cl = mClassLoader;
				CLLoop:
				while (cl != null) {
					 for (String cls : c.classes) {
						  try {
								cl.loadClass(cls);
						  } catch (ClassNotFoundException e) {
								Log.i(LOG_TAG, String.format(
									 "ClassLoader %s couldn't load %s",
									 cl.getClass().getName(), cls
								));
								while(true) {
									 try {
										  cl = cl.getParent();
										  break;
									 } catch (SecurityException ex) {
										  continue;
									 }
								}
								continue CLLoop;
						  }
							Log.i(LOG_TAG, String.format(
								 "ClassLoader %s could load %s",
								 cl.getClass().getName(), cls
							));
					 }
					 return true;
				}
				return false;
		  }


		  private void dumpChainToFile(String p, GadgetChain c) {
				PrintWriter pw = null;
				try {
					 File f = new File(p);
					 pw = new PrintWriter(new BufferedWriter(new FileWriter(f, true)));
					 if (!f.exists()) {
						  f.createNewFile();
					 }
					 pw.printf("--- %s ---\n", c.name);
					 pw.println("Dependencies:");
					 for (String dep : c.dependencies) {
						  pw.printf("   - %s\n", dep);
					 }
					 pw.println("Classes:");
					 for (String cls : c.classes) {
						  pw.printf("   - %s\n", cls);
					 }
					 pw.printf("--- End %s ---\n", c.name);
				} catch (Exception e) {
					 Log.e(LOG_TAG, "Failed to dump chain to file", e);
				} finally {
					 if (pw != null) {
						  pw.close();
					 }
				}
		  }

		  public static class GadgetChain {
				public final String name;
				public final String[] dependencies;
				public final String[] classes;

				public GadgetChain(String pName, String[] deps, String[] pClasses) {
					 name = pName;
					 dependencies = deps;
					 classes = pClasses;
				}
		  }
	 }
}
