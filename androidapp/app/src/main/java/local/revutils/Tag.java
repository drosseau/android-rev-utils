package local.revutils;

public enum Tag {
    DEBUG,
    INFO,
    WARNING,
    ERROR,
    VERBOSE;
}

