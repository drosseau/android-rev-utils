package local.revutils;

import android.os.Bundle;
import android.os.Environment;

import android.content.Intent;
import android.content.IntentFilter;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.BufferedWriter;

import java.lang.Iterable;
import java.lang.StringBuilder;

import java.nio.ByteBuffer;

import java.util.Set;
import java.util.UUID;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

public class Logger {
	 private String mTag;
	 private static final char[] HEX_CHARS = "0123456789abcdef".toCharArray();

     public Logger(String tag) {
         mTag = tag;
     }

	 public void dumpStackTrace() {
		  StackTraceElement[] elems = Thread.currentThread().getStackTrace();
		  String myClass = Utils.class.getName();
		  int i = 0;
		  while(i < elems.length) {
				StackTraceElement el = elems[i];
				String cls = el.getClassName();
				if (cls.equals(myClass)) {
					 break;
				}
				i++;
		  }
		  while(i < elems.length) {
				StackTraceElement el = elems[i];
				String cls = el.getClassName();
				if (!(cls.equals(myClass))) {
					 break;
				}
				i++;
		  }
		  for (; i < elems.length; i++) {
				Utils.doLog(mTag, Tag.INFO, String.format("%s", elems[i]));
		  }
	 }

	 public void logUuids(UUID[] uuids) {
		  logUuids("UUID is", uuids);
	 }

	 public void logUuids(String pre, UUID[] uuids) {
		  for (UUID uuid : uuids) {
				String s = String.format("%s: %s", pre, uuid);
				Utils.doLog(mTag, Tag.INFO, s);
		  }
	 }

	 public void logBytes(String s, ByteBuffer buf) {
		  logBytes(s, buf.array());
	 }

	 public void logBytes(ByteBuffer buf) {
		  logBytes(buf.array());
	 }

	 public void logBytes(byte[] b) {
		  logBytes("Bytes are", b);
	 }

	 public void logBytes(String s, byte[] b) {
		  String out = String.format("%s: %s", s, bytesToHexString(b));
		  Utils.doLog(mTag, Tag.INFO, out);
	 }

	 public void logWarningTagged(String s) {
		  logTagged(Tag.WARNING, s);
	 }

	 public void logInfoTagged(String s) {
		  logTagged(Tag.INFO, s);
	 }

	 public void logErrorTagged(String s) {
		  logTagged(Tag.ERROR, s);
	 }

	 public void logDebugTagged(String s) {
		  logTagged(Tag.DEBUG, s);
	 }

	 public void logVerboseTagged(String s) {
		  logTagged(Tag.VERBOSE, s);
	 }

	 public void logWarningTagged(Throwable e) {
		  logTagged(Tag.WARNING, "", e);
	 }

	 public void logInfoTagged(Throwable e) {
		  logTagged(Tag.INFO, "", e);
	 }

	 public void logErrorTagged(Throwable e) {
		  logTagged(Tag.ERROR, "", e);
	 }

	 public void logDebugTagged(Throwable e) {
		  logTagged(Tag.DEBUG, "", e);
	 }

	 public void logVerboseTagged(Throwable e) {
		  logTagged(Tag.VERBOSE, "", e);
	 }

	 public void logDebugTagged(String s, Throwable e) {
		  logTagged(Tag.DEBUG, s, e);
	 }

	 public void logVerboseTagged(String s, Throwable e) {
		  logTagged(Tag.VERBOSE, s, e);
	 }

	 public void logInfoTagged(String s, Throwable e) {
		  logTagged(Tag.INFO, s, e);
	 }

	 public void logWarningTagged(String s, Throwable e) {
		  logTagged(Tag.WARNING, s, e);
	 }

	 public void logErrorTagged(String s, Throwable e) {
		  logTagged(Tag.ERROR, s, e);
	 }

	 public void logDebugTagged(Throwable e, String s) {
		  logTagged(Tag.DEBUG, s, e);
	 }

	 public void logVerboseTagged(Throwable e, String s) {
		  logTagged(Tag.VERBOSE, s, e);
	 }

	 public void logInfoTagged(Throwable e, String s) {
		  logTagged(Tag.INFO, s, e);
	 }

	 public void logWarningTagged(Throwable e, String s) {
		  logTagged(Tag.WARNING, s, e);
	 }

	 public void logErrorTagged(Throwable e, String s) {
		  logTagged(Tag.ERROR, s, e);
	 }

	 public void logWarningTagged(String fmt, Object... objs) {
		  logTagged(Tag.WARNING, fmt, objs);
	 }

	 public void logInfoTagged(String fmt, Object... objs) {
		  logTagged(Tag.INFO, fmt, objs);
	 }

	 public void logErrorTagged(String fmt, Object... objs) {
		  logTagged(Tag.ERROR, fmt, objs);
	 }

	 public void logDebugTagged(String fmt, Object... objs) {
		  logTagged(Tag.DEBUG, fmt, objs);
	 }

	 public void logVerboseTagged(String fmt, Object... objs) {
		  logTagged(Tag.VERBOSE, fmt, objs);
	 }

	 public void logDebugTagged(Throwable e, String fmt, Object... objs) {
		  logTagged(Tag.DEBUG, fmt, e, objs);
	 }

	 public void logVerboseTagged(Throwable e, String fmt, Object... objs) {
		  logTagged(Tag.VERBOSE, fmt, e, objs);
	 }

	 public void logInfoTagged(Throwable e, String fmt, Object... objs) {
		  logTagged(Tag.INFO, fmt, e, objs);
	 }

	 public void logWarningTagged(Throwable e, String fmt, Object... objs) {
		  logTagged(Tag.WARNING, fmt, e, objs);
	 }

	 public void logErrorTagged(Throwable e, String fmt, Object... objs) {
		  logTagged(Tag.ERROR, fmt, e, objs);
	 }

	 public void logDebugTagged(String fmt, Throwable e, Object... objs) {
		  logTagged(Tag.DEBUG, fmt, e, objs);
	 }

	 public void logVerboseTagged(String fmt, Throwable e, Object... objs) {
		  logTagged(Tag.VERBOSE, fmt, e, objs);
	 }

	 public void logInfoTagged(String fmt, Throwable e, Object... objs) {
		  logTagged(Tag.INFO, fmt, e, objs);
	 }

	 public void logWarningTagged(String fmt, Throwable e, Object... objs) {
		  logTagged(Tag.WARNING, fmt, e, objs);
	 }

	 public void logErrorTagged(String fmt, Throwable e, Object... objs) {
		  logTagged(Tag.ERROR, fmt, e, objs);
	 }

	 public void logTagged(Tag tag, String s) {
		  if (s == null) {
				Utils.doLog(mTag, Tag.ERROR, "given a null string to log");
				return;
		  }
		  Utils.doLog(mTag, tag, s);
	 }
	 public void logTagged(Tag tag, String s, Throwable e) {
		  if (s == null) {
				Utils.doLog(mTag, Tag.ERROR, "given a null string to log");
				return;
		  }
		  Utils.doLog(mTag, tag, s, e);
	 }

	 public void logTagged(Tag tag, String fmt, Object... objs) {
		  if (fmt == null) {
				Utils.doLog(mTag, Tag.ERROR, "given a null fmt string");
				return;
		  }
		  String out = String.format(fmt, objs);
		  Utils.doLog(mTag, tag, out);
	 }

	 public void logTagged(Tag tag, String fmt, Throwable e, Object... objs) {
		  if (fmt == null) {
				Utils.doLog(mTag, Tag.ERROR, "given a null fmt string");
				return;
		  }
		  String out = String.format(fmt, objs);
		  Utils.doLog(mTag, tag, out, e);
	 }

	 public void logf(String fmt, Object... objs) {
		  if (fmt == null) {
				Utils.doLog(mTag, Tag.ERROR, "given a null fmt string");
				return;
		  }
		  String out = String.format(fmt, objs);
		  Utils.doLog(mTag, Tag.INFO, out);
	 }

	 public void logByte(int idx, byte[] bs) {
         if (idx < 0 || idx >= bs.length) {
             Utils.doLog(mTag, Tag.ERROR, String.format("Can't log byte at %d", idx));
             return;
         }
         logByte(idx, bs[idx]);
	 }

	 public void logByte(int idx, byte b) {
		  char[] hex = {
				HEX_CHARS[b >> 4],
				HEX_CHARS[b & 0xF],
		  };
		  String out = String.format("Byte at %d is %s", idx, new String(hex));
		  Utils.doLog(mTag, Tag.INFO, out);
	 }
	 public String bytesToHexString(byte[] bs) {
		  char[] hex = new char[bs.length * 2];
		  for (int i = 0; i < bs.length; i++) {
				int bv = bs[i] & 0xFF;
				hex[i << 1] = HEX_CHARS[bv >> 4];
				hex[(i << 1) + 1] = HEX_CHARS[bv & 0xF];
		  }
		  return new String(hex);
	 }

	 /**
	  * These functions help deal with SLF4J trying to be clever. They don't
	  * cover everything though. Check here:
	  * https://www.slf4j.org/apidocs/index.html
	  */
	 public void logWarningTagged(String fmt, Object arg1) {
		  if (fmt == null) {
				Utils.doLog(mTag, Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logWarningTagged(String.format(fmt, arg1));
	 }
	 public void logWarningTagged(String fmt, Object arg1, Object arg2) {
		  if (fmt == null) {
				Utils.doLog(mTag, Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logWarningTagged(String.format(fmt, arg1, arg2));
	 }
	 public void logErrorTagged(String fmt, Object arg1) {
		  if (fmt == null) {
				Utils.doLog(mTag, Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logErrorTagged(String.format(fmt, arg1));
	 }
	 public void logErrorTagged(String fmt, Object arg1, Object arg2) {
		  if (fmt == null) {
				Utils.doLog(mTag, Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logErrorTagged(String.format(fmt, arg1, arg2));
	 }
	 public void logInfoTagged(String fmt, Object arg1) {
		  if (fmt == null) {
				Utils.doLog(mTag, Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logInfoTagged(String.format(fmt, arg1));
	 }
	 public void logInfoTagged(String fmt, Object arg1, Object arg2) {
		  if (fmt == null) {
				Utils.doLog(mTag, Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logInfoTagged(String.format(fmt, arg1, arg2));
	 }

	 public void logDebugTagged(String fmt, Object arg1) {
		  if (fmt == null) {
				Utils.doLog(mTag, Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logDebugTagged(String.format(fmt, arg1));
	 }
	 public void logDebugTagged(String fmt, Object arg1, Object arg2) {
		  if (fmt == null) {
				Utils.doLog(mTag, Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logDebugTagged(String.format(fmt, arg1, arg2));
	 }

	 public void logVerboseTagged(String fmt, Object arg1) {
		  if (fmt == null) {
				Utils.doLog(mTag, Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logVerboseTagged(String.format(fmt, arg1));
	 }
	 public void logVerboseTagged(String fmt, Object arg1, Object arg2) {
		  if (fmt == null) {
				Utils.doLog(mTag, Tag.ERROR, "given a null fmt string");
				return;
		  }
		  logVerboseTagged(String.format(fmt, arg1, arg2));
	 }

	 public void dumpIntentFilter(IntentFilter filter) {
		  Utils.doLog(mTag, Tag.INFO, "Dumping IntentFilter");
		  if (filter == null) {
				Utils.doLog(mTag, Tag.INFO, "IntentFilter was null");
				return;
		  }
		  dumpIt(filter.actionsIterator(), "Action: %s");
		  dumpIt(filter.categoriesIterator(), "Category: %s");
		  dumpIt(filter.pathsIterator(), "Path: %s");
		  dumpIt(filter.schemesIterator(), "Scheme: %s");
		  dumpIt(filter.schemeSpecificPartsIterator(), "Scheme Spec Part: %s");
		  dumpIt(filter.typesIterator(), "Type: %s");
		  dumpIt(filter.authoritiesIterator(), "Authority: %s");
	 }

	 private <T> void dumpIt(Iterator<T> it, String fmt) {
		  if (it != null && it.hasNext()) {
				for (T e : makeIterable(it)) {
					 Utils.doLog(mTag, Tag.INFO, String.format(fmt, e));
				}
		  }
	 }

	 private static <T> Iterable<T> makeIterable(final Iterator<T> it) {
		  return new Iterable<T>() { public Iterator<T> iterator() { return it; } };
	 }

	 public void dumpIntent(Intent intent) {
		  Utils.doLog(mTag, Tag.INFO, "Dumping intent");
		  if (intent == null) {
				Utils.doLog(mTag, Tag.INFO, "Intent was null");
				return;
		  }
		  if (intent.getAction() != null) {
				Utils.doLog(mTag, Tag.INFO, String.format("Action: %s", intent.getAction()));
		  }
		  if (intent.getDataString() != null) {
				Utils.doLog(mTag, Tag.INFO, String.format("Data: %s", intent.getDataString()));
		  }
		  if (intent.getPackage() != null) {
				Utils.doLog(mTag, Tag.INFO, String.format("Package: %s", intent.getPackage()));
		  }
		  Set<String> categories = intent.getCategories();
		  if (categories != null && categories.size() > 0) {
				Utils.doLog(mTag, Tag.INFO, "Categories:");
				for (String s : categories) {
					 Utils.doLog(mTag, Tag.INFO, String.format("\t%s", s));
				}
		  }
		  Bundle bundle = intent.getExtras();
		  if (bundle != null) {
				Set<String> keys = bundle.keySet();
				if (keys != null && keys.size() > 0) {
					 Utils.doLog(mTag, Tag.INFO, "Extras:");
					 for (String k : keys) {
						  Object val = bundle.get(k);
						  if (val != null) {
								Utils.doLog(mTag, Tag.INFO, String.format(
									 "Key: %s, Val: %s, Class: %s",
									 k, val.toString(), val.getClass().getName()
								));
						  } else {
								Utils.doLog(mTag, Tag.INFO, String.format("Key %s is null", k));
						  }
					 }
				}
		  }
	 }
}
